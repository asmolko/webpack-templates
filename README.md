# Webpack Template + UIkit Icons

### Getting started

Generating UIkit Icons:

- `cd uikit-icons`
- `npm i`
- insert .svg files of icons to `uikit-icons/icons` directory
- `npm run compile-js` for compile icons bundle
- `npm run compile` for compile js and css bundles

[![asciicast](https://asciinema.org/a/wDkPIm0zfcKtTwuvh3iQdSmb2.svg)](https://asciinema.org/a/wDkPIm0zfcKtTwuvh3iQdSmb2)

