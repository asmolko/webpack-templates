const util = require('./util');
const argv = require('minimist')(process.argv.slice(2));

argv._.forEach(arg => {
    const tokens = arg.split('=');
    argv[tokens[0]] = tokens[1] || true;
});

const numArgs = Object.keys(argv).length;
argv.all = argv.all || numArgs <= 1; // no arguments passed, so compile all

const minify = !(argv.debug || argv.nominify || argv.d);

const steps = {
    core: () => util.compile('src/js/uikit-core.js', 'dist/js/uikit-core', {minify}),
    icons: async () => util.compile('build/wrapper/icons.js', 'dist/js/uikit-icons', {
        minify,
        name: 'icons',
        replaces: {ICONS: await util.icons('{src/icons,icons}/*.svg')}}
    ),
    index: () => util.write('dist/js/index.js', 'import UIkit from \'./uikit-core\';\nimport Icons from \'./uikit-icons\';\n\nUIkit.use(Icons);')
};

if (argv.h || argv.help) {

    console.log(`
        usage:

        build.js [componentA, componentB, ...] [-d|debug|nominify|development]

        examples:

        build.js // builds all of uikit, including icons and does minification (implies 'all')
        build.js uikit icons -d // builds all of uikit and the icons, skipping the minification
        build.js core lightbox -d // builds uikit-core and the lightbox, skipping the minification

        bundles: ${Object.keys(steps).join(', ')}
    `);

} else {

    let jobs = collectJobs();

    if (jobs.length === 0) {
        argv.all = true;
        jobs = collectJobs();
    }

}

function collectJobs() {

    if (argv.all) {
        Object.assign(argv, steps);
    }

    return Object.keys(argv)
        .filter(step => steps[step])
        .map(step =>
            steps[step]()
                .catch(({message}) => {
                    console.error(message);
                    process.exitCode = 1;
                })
    );
}
