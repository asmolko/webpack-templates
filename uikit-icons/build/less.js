const util = require('./util');
const argv = require('minimist')(process.argv);

argv._.forEach(arg => {
    const tokens = arg.split('=');
    argv[tokens[0]] = tokens[1] || true;
});

const develop = argv.develop || argv.debug || argv.d || argv.nominify;

[{src: 'src/less/uikit.less', dist: `dist/css/uikit-core.css`}].forEach(config => compile(config.src, config.dist));

async function compile(file, dist) {

    const less = await util.read(file);

    let output = (await util.renderLess(less, {
        relativeUrls: true,
        rootpath: '../../',
        paths: ['src/less/']
    })).replace(/\.\.\/dist\//g, '');

    const res = await util.write(dist, util.banner + output);

    if (!develop) {
        util.minify(res);
    }
}
