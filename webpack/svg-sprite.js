const SpriteLoaderPlugin = require('svg-sprite-loader/plugin')

module.exports = function () {
    return {
        plugins: [
            new SpriteLoaderPlugin({ plainSprite: true })
        ],

        module: {
            rules: [
                {
                    test: /\.svg$/,
                    exclude: [
                        /images\/svg\/.+\.svg$/,
                    ],
                    loader: 'svg-sprite-loader',
                    options: {
                        extract: true,
                        spriteFilename: (svgPath) => `images/sprite/sprite${ svgPath.substr(-4) }`
                    }
                }
            ]
        }
    }
}
