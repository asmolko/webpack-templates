const fs                    = require('fs')
const path                  = require('path')
const HtmlWebpackPlugin     = require('html-webpack-plugin')
                              require('dotenv').config()

module.exports = function () {
    return {
        plugins: [
            ...fs
                .readdirSync(path.resolve(__dirname, `.${process.env.CONTEXT_PATH}/pug/pages`))
                .filter(fileName => fileName.endsWith('.pug'))
                .map(page => new HtmlWebpackPlugin({
                    filename: `${path.parse(page).name}.html`,
                    template: `./pug/pages/${page}`,
                    minify: false,
                    inject: 'body',
                }))
        ],
        module: {
            rules: [
                {
                    test: /\.pug$/,
                    include: path.resolve(__dirname, `.${process.env.CONTEXT_PATH}/pug`),
                    use: {
                        loader: 'pug-loader',
                        options: {
                            pretty: true
                        }
                    }
                }
            ]
        }
    }
}
