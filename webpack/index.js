const scripts       = require('./script')
const styles        = require('./styles')
const images        = require('./images')
const fonts         = require('./fonts')
const svgSprite     = require('./svg-sprite')
const pug           = require('./pug')

module.exports = [
  scripts(),
  styles(),
  images(),
  fonts(),
  pug(),
  svgSprite()
]
